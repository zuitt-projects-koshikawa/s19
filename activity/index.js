let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"],
}


	// Traditional Function
/*function introduce(student){

	const {name, birthday, age, isEnrolled, classes} = student
	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}
*/
 
		 // Note for named functions, fat arrow is treated the arrow like variables
		 // syntax:  declaration name = parameter => body;
		 // sample: let num = 1 => num + 200;


const introduce = student => {
	const {name, birthday, age, isEnrolled, classes} = student
	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
};




introduce(student1);
// introduce(student2);


// Traditional Function
/*function getCube(num){
	console.log(Math.pow(num, 3));
};

let cube = getCube(3);

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
});

let numSquared = numArr.map(function(num){
	return num ** 2;
});
console.log(numSquared);
*/


let getCube = num => console.log(Math.pow(num,3));
let cube = getCube(3);

console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(num => console.log(num));
let numSquared = numArr.map(num => num ** 2);
console.log(numSquared);


class Dog {
	constructor(name, breed, age){
	this.Name = name;
	this.Breed = breed;
	this.Age = age;
	}
};

let dog1 = new Dog("Swimpie", "Shih Tzu", 1*7);
let dog2 = new Dog("Cedric", "Bull Dog", 5*7);

console.log(dog1);
console.log(dog2);



